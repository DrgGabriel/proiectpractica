﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectPractica
{
	class Board
	{
		private int[] board = new int[8];

		public Board()
		{
			for (int i = 0; i < 8; i++)
			{
				board[i] = 0;
			}
		}
		public void setCell(int cellParam, int cellType)
		{
			board[cellParam] = cellType;
		}

		public int getCell(int cellParam)
		{
			return board[cellParam];
		}
	}
}
