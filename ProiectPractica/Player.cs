﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectPractica
{
	class Player
	{
		private int PlayerType;
		private string PlayerName;

		Player()
		{
			PlayerType = 0;
			PlayerName = string.Empty;
		}

		Player(string nameParam, int typeParam)
		{
			PlayerName = nameParam;
			PlayerType = typeParam;
		}
	}
}
